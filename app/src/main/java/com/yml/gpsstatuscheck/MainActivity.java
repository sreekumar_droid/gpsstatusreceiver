package com.yml.gpsstatuscheck;

import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements GpsStatusReceiver.GspStatusCallback {
    private static final String TAG = "MainActivity";
    private GpsStatusReceiver mGpsStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGpsStatusReceiver = new GpsStatusReceiver(this, getApplicationContext());
        getApplicationContext().registerReceiver(mGpsStatusReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));

    }

    @Override
    protected void onResume() {
        super.onResume();
        getApplicationContext().registerReceiver(mGpsStatusReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();

        getApplicationContext().unregisterReceiver(mGpsStatusReceiver);
    }

    @Override
    public void onGpsStatusChanged(boolean status) {
        Log.d(TAG, "GPS status :" + status);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGpsStatusReceiver.removeCallback();
    }
}
