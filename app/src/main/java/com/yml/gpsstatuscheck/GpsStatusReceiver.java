package com.yml.gpsstatuscheck;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by YMediaLabs
 * <p/>
 * Copyright (C) 2016
 */

/**
 * Wrapper class for avoiding multiple callback while switching the GPS status.
 *
 * Issue details:
 * http://stackoverflow.com/questions/15452084/android-detect-when-gps-is-turned-on-off-or-when-no-app-is-using-it-anymore
 *
 */
public class GpsStatusReceiver extends BroadcastReceiver {

    private static final String TAG = "GpsStatusReceiver";

    /**
     * Wrapper callback to avoid multiple callback while switch the status of GPS
     *
     */
    public interface GspStatusCallback{
        /**
         *
         * Callback for the GPS status
         *
         * @param status current status of the GPS
         */
        void onGpsStatusChanged(final boolean status);
    }

    private  GspStatusCallback mCallback;
    private boolean mGpsStatus;
    private LocationManager mManager;

    public GpsStatusReceiver(@NonNull final GspStatusCallback callback,final Context applicationContext){
        mCallback = callback;
        mManager = (LocationManager)  applicationContext.getSystemService(Context.LOCATION_SERVICE );
        mGpsStatus = mManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


    }

    /**
     * This method is called when the BroadcastReceiver is receiving an Intent
     * broadcast.  During this time you can use the other methods on
     * BroadcastReceiver to view/modify the current result values.  This method
     * is always called within the main thread of its process, unless you
     * explicitly asked for it to be scheduled on a different thread using
     * {@link Context#registerReceiver(BroadcastReceiver,
     * IntentFilter, String, Handler)}. When it runs on the main
     * thread you should
     * never perform long-running operations in it (there is a timeout of
     * 10 seconds that the system allows before considering the receiver to
     * be blocked and a candidate to be killed). You cannot launch a popup dialog
     * in your implementation of onReceive().
     * <p/>
     * <p><b>If this BroadcastReceiver was launched through a &lt;receiver&gt; tag,
     * then the object is no longer alive after returning from this
     * function.</b>  This means you should not perform any operations that
     * return a result to you asynchronously -- in particular, for interacting
     * with services, you should use
     * {@link Context#startService(Intent)} instead of
     * {@link Context#bindService(Intent, ServiceConnection, int)}.  If you wish
     * to interact with a service that is already running, you can use
     * {@link #peekService}.
     * <p/>
     * <p>The Intent filters used in {@link Context#registerReceiver}
     * and in application manifests are <em>not</em> guaranteed to be exclusive. They
     * are hints to the operating system about how to find suitable recipients. It is
     * possible for senders to force delivery to specific recipients, bypassing filter
     * resolution.  For this reason, {@link #onReceive(Context, Intent) onReceive()}
     * implementations should respond only to known actions, ignoring any unexpected
     * Intents that they may receive.
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "Intent Action :" + intent.getAction());

       final boolean currentStatus =  mManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(currentStatus!=mGpsStatus){
            mGpsStatus = currentStatus;
            if(mCallback!=null)
                mCallback.onGpsStatusChanged(mGpsStatus);
        }


    }

    /**
     * Cleanup method
     */
    public void removeCallback(){
        mCallback = null;
    }
}
